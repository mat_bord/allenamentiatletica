<?php
session_start();

function deletelement($idcartella,$conn){
    //cancella programmi nella cartella
    $queryelem = "SELECT id FROM programmaallenamento WHERE cartella=".$idcartella. "";
    if (!empty($queryelem)){
        $riselem = $conn->query($queryelem);
        while($rigaelem=$riselem->fetch_assoc()){ 

            //rimuovo elemento
            $query = "SELECT percorso FROM programmaallenamento WHERE id=".$rigaelem["id"]."";
            $ris = $conn->query($query);
            $riga=$ris->fetch_assoc(); 
            unlink($riga['percorso']);


            // cancello la riga
            $sql = "DELETE FROM programmaallenamento WHERE id=".$rigaelem["id"]."";

            $result = $conn->query($sql);
        }
    }
}

function deleteinlink($idcartella,$conn){
        //cancella collegamenti alle cartelle dentro la cartella da cancellare
        $querycoll = "SELECT id FROM linkcartella WHERE idcontenitore=".$idcartella. "";
        if (!empty($querycoll)){
            $riscoll = $conn->query($querycoll);
    
            while($rigacoll=$riscoll->fetch_assoc()){ 
    
                // sql to delete a record
                $sql = "DELETE FROM linkcartella WHERE id=".$rigacoll["id"]."";
    
                $result = $conn->query($sql);
            }
        }
}

function deleteoutlink($idcartella,$conn){
    
    //cancella collegamenti alle cartelle riferiti alla cartella da cancellare
    $querycoll = "SELECT id FROM linkcartella WHERE idriferimento=".$idcartella. "";
    if (!empty($querycoll)){
        $riscoll = $conn->query($querycoll);

        while($rigacoll=$riscoll->fetch_assoc()){ 

            // sql to delete a record
            $sql = "DELETE FROM linkcartella WHERE id=".$rigacoll["id"]."";

            $result = $conn->query($sql);
        }
    }
}

function deleteelementolink($idcartella,$conn){
    
    //cancella collegamenti agli elementi riferiti alla cartella da cancellare
    $querycoll = "SELECT id FROM linkprogrammaallenamento WHERE cartellacontenitore=".$idcartella."";
    if (!empty($querycoll)){
        $riscoll = $conn->query($querycoll);

        while($rigacoll=$riscoll->fetch_assoc()){ 

            // sql to delete a record
            $sql = "DELETE FROM linkprogrammaallenamento WHERE id=".$rigacoll["id"]."";

            $result = $conn->query($sql);
        }
    }
}


function delete($idcartella,$conn) {
    $querysottocartella="SELECT id FROM cartella WHERE padre=".$idcartella."";
    //se esistono sottocartelle invoco la funzione per ogni sottocartella fino ad arrivare alla più profonda
    if (!empty($querysottocartella)){       
        $rissottocartella = $conn->query($querysottocartella);
        //per ogni sottocartella invoco la funzione per vedere se ce ne sono
        while($rigasottocartella=$rissottocartella->fetch_assoc()){ 
            delete($rigasottocartella["id"],$conn); 
            //quando ritorno, la sottocartella avrà perso i suoi sottoelementi/cartelle quindi posso eliminarla
            $sqlcart = "DELETE FROM cartella WHERE id=".$rigasottocartella["id"]."";
            $conn->query($sqlcart);
        }
        //ora che non ci sono più sottocartelle posso cancellare gli elementi dell'attuale cartella
        deleteelementolink($idcartella,$conn);
        deletelement($idcartella,$conn);
        deleteinlink($idcartella,$conn);
        deleteoutlink($idcartella,$conn);   
         
    }
    else{//se non ci sono sottocartelle
        deleteelementolink($idcartella,$conn);   
        deletelement($idcartella,$conn);
        deleteinlink($idcartella,$conn);
        deleteoutlink($idcartella,$conn);  
          
    }
}

include 'connectiondb.php';

$result = $conn->query("SET NAMES 'utf8'");


$idcartella=$_POST["Idcartella"];

delete($idcartella,$conn);

// cancella cartella originaria
$sqlcart = "DELETE FROM cartella WHERE id=".$idcartella."";

// if (!mysqli_query($conn,$sqlcart))
// {
//  echo("Error description: " . mysqli_error($con));
// }

$cartellacanc=0;
if ($conn->query($sqlcart) === TRUE) {
    $cartellacanc=1;
} else {
    $cartellacanc=0;
}



if ($cartellacanc==1) {
    $_SESSION['Info']='cartellacancellata';
    header('Location:index.php');
} else {
    $_SESSION['Info']='cartellanoncancellata';
    header('Location:index.php');
}

$conn->close();

?>