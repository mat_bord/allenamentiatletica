<?php
session_start();


// per prima cosa verifico che il file sia stato effettivamente caricato
if ( (filesize($_FILES['userfile']['tmp_name'])> 20000000) || !isset($_FILES['userfile']) || !is_uploaded_file($_FILES['userfile']['tmp_name']) ) {
  $_SESSION['Info']='errfile';
  header("location: upload.php");
  exit;    
}

//percorso della cartella dove mettere i file caricati dagli utenti
$uploaddir = 'images/';

//Recupero il percorso temporaneo del file
$userfile_tmp = $_FILES['userfile']['tmp_name'];

//recupero il nome originale del file caricato
$userfile_name = $_FILES['userfile']['name'];
//rimuovo apici
$userfile_name=addslashes($userfile_name);

$path=$uploaddir;
$filename=$userfile_name;

//rinomina il file se già esistente
if ($pos = strrpos($filename, '.')) {
  $name = substr($filename, 0, $pos);
  $ext = substr($filename, $pos);
} else {
  $name = $filename;
}

$newpath = $path.'/'.$filename;
$newname = $filename;
$counter = 0;
while (file_exists($newpath)) {
  $newname = $name .'_'. $counter . $ext;
  $newpath = $path.'/'.$newname;
  $counter++;
}

$userfile_name=$newname;

//copio il file dalla sua posizione temporanea alla mia cartella upload
$fine=move_uploaded_file($userfile_tmp, $uploaddir . $userfile_name);


include 'connectiondb.php';

$result = $conn->query("SET NAMES 'utf8'");


$todaydate=date("Y-m-d");
$nome=addslashes($_POST['nome']);
$commento=addslashes($_POST['commento']);

$sql = "INSERT INTO programmaallenamento (percorso, nome, tipo, allenatore, commento, cartella, dataupload)
VALUES ('images/".$userfile_name."','".$nome."', '".$_POST['tipo']."', '".$_SESSION['username']."', '".$commento."', '".$_POST['cartella']."', '".$todaydate."')";

if ($conn->query($sql) === TRUE) {
  $_SESSION['Info']='elementocaricato';
  header("location: upload.php");
} else {
  $_SESSION['Info']='errelementocaricato';
  header("location: upload.php");
}

$conn->close();


?>