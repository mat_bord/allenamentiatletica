<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Atletica Montebelluna allenamenti</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" media="screen" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="assets/SnackBar/dist/snackbar.css" />
		<script src="assets/SnackBar/dist/snackbar.js"></script>
		<script language="JavaScript" type="text/javascript">
			function checkDelete(){
				return confirm('Sei sicuro di cancellare?');
			}

			function popup(){
				var popupCounts = <?php echo json_encode($_SESSION['Info']); ?>;
				if(popupCounts=='elementocancellato'){
					Snackbar.show({text:'Elemento cancellato', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='elementononcancellato'){
					Snackbar.show({text:'Elemento NON cancellato', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='cartellacreata'){
					Snackbar.show({text:'Cartella creata', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='modificaok'){
					Snackbar.show({text:'Elemento modificato', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='nonmodifica'){
					Snackbar.show({text:'Elemento NON modificato', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='cartellacancellata'){
					Snackbar.show({text:'Cartella cancellata', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='cartellanoncancellata'){
					Snackbar.show({text:'Cartella NON cancellata', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='modificacartellaok'){
					Snackbar.show({text:'Cartella modificata', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='nonmodificacartella'){
					Snackbar.show({text:'Cartella NON modificata', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='modificalinkok'){
					Snackbar.show({text:'Link modificato', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='nonmodificalink'){
					Snackbar.show({text:'Link NON modificato', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='linkcancellato'){
					Snackbar.show({text:'Link eliminato', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='linknoncancellato'){
					Snackbar.show({text:'Link NON eliminato', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				<?php $_SESSION['Info']='None'; ?>
			}		



			function insertParam(key, value)
				{
					key = encodeURI(key); value = encodeURI(value);

					var kvp = document.location.search.substr(1).split('&');

					var i=kvp.length; var x; while(i--) 
					{
						x = kvp[i].split('=');

						if (x[0]==key)
						{
							x[1] = value;
							kvp[i] = x.join('=');
							break;
						}
					}

					if(i<0) {kvp[kvp.length] = [key,value].join('=');}

					//this will reload the page, it's likely better to store this until finished
					document.location.search = kvp.join('&'); 
				}

		</script>

	
	</head>
	<body class="is-preload" onload="popup()">
                        
		<!-- Wrapper -->
		<div id="wrapper">
			<?php
				if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
					echo"
					<div id='enterbtncontainer'>
						<a href='upload.php'><button type='button' class=' btn btn-default '>Carica</button></a>
						<a href='logout.php'><button type='button' class='exit btn btn-default '>Esci</button></a>			
					</div>";		
				}
				else{
					echo"<div id='enterbtncontainer'>
						<a href='login.php'><button type='button' class=' btn btn-default '>Login</button></a>			
					</div>	";
				}		
			?>
				<!-- Header -->
					<header id="header">
					
						<div class="logo">
							<span class="icon"> <img id="logo" src="images/logo.png"></span>
						</div>
						<div class="content">
							<div class="inner">
								<h1>Atletica Montebelluna raccolta allenamenti</h1>
								<p>In questo sito troverete tutti i file relativi agli allenamenti.</p>
							</div>
						</div>
						<nav>
							<ul>
								<li ><a href="filtri.php?tipo=Circuiti">Circuiti</a></li>
								<li ><a href="filtri.php?tipo=Programmi">Programmi</a></li>
								<li ><a href="filtri.php?tipo=Esercizi">Esercizi</a></li>
							</ul>
						</nav>
					</header>
					
				
					
				<div id="totcards">						
					<?php
						include 'connectiondb.php';

						$result = $conn->query("SET NAMES 'utf8'");

						$sql = "SELECT id, nome, dataupload, commento FROM cartella WHERE padre IS NULL ORDER BY dataupload DESC";
						$result = $conn->query($sql);
						
						echo "<div class='card-deck' >";
						// output data of each row
						$count=0;
						while($row = $result->fetch_assoc()) {
							$count=$count+1;
							if($count>3){
								echo"
									</div>
									<div class='card-deck' >";
								$count=1;
							}
							// Card visualization 
							echo "
								<div  class='card'>
									<a class='linkcard' href='#".$row["id"]."' >
										<div class='card-body'>
											<h5 class='card-title' >".$row["nome"]."</h5>
											<p class='card-text' >".$row["commento"]."</p>
										</div>
										<div class='card-footer'>
											<small class='text-muted'>".$row["dataupload"]."</small>
											<div class='bottonicartella'>";
											if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
												echo" 
													<form method='post' action='modificacartella.php'>
														<input type='hidden' id='Idcartella' name='Idcartella' value='".$row["id"]."'>
														<button type='submit' class='btncartella btn'><i class='fa fa-folder'></i> Modifica</button>
													</form>
													<form method='post' action='cancellacartella.php'>
														<input type='hidden' id='Idcartella' name='Idcartella' value='".$row["id"]."'>
														<button class='btncartella btn' onclick='return checkDelete()'><i class='fa fa-trash-o'></i>Elimina</button>
													</form>";
												}
											echo"
											</div>
										</div>
									</a>
								</div>";
												
						}
						while ($count<3){
							$count=$count+1;
							echo("
									<div class='card hiddencard' >
										<img class='card-img-top'> 
										<div class='card-body' >
											<h5 class='card-title' ></h5>
											<p class='card-text' ></p>
										</div>
										<div class='card-footer'>
											<small class='text-muted'></small>
										</div>
									</div>");
						}

						echo"</div>";
						?>
				
				</div>

				<!-- <div id="overlay" onclick="off()">
				info from the card that the user clicked 					
				</div>  -->
 
				<!-- Main -->
					<div id="main">
						
						<?php
							$sql = "SELECT id ,nome FROM cartella ";
							$resultcart = $conn->query($sql);
							while($cartella = $resultcart->fetch_assoc()) {
								$mappasito=percorso($cartella["id"],$cartella["nome"],$conn);
								echo "<article id='".$cartella["id"]."'>
										<p id='mappasito'>
											".$mappasito."
										</p>
										<div class='backbutton'>
											<a href='".getpadre($cartella["id"],$conn)."' class='previous round'>&#8249;</a>
										</div>";

								//Visualizzazione linkcartelle
								$sqllink = "SELECT id,nome, idriferimento, commento FROM linkcartella WHERE idcontenitore='".$cartella["id"]."' ORDER BY dataupload ";
								$resultlink = $conn->query($sqllink);
								
								while( $rowlink = $resultlink->fetch_assoc()){
									echo"
									<div class='sottocartella'>
										<a class='linkcard' href='#".$rowlink["idriferimento"]."' >
										<div class='btnsubcart'> 
											<div class='tipoelemento'>
												Collegamento cartella
											</div>
											<div class='subcartname'>
												<i class='fa fa-folder fa-4x'></i>".$rowlink["nome"]."
											</div>";
											if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
												echo" 				
												<form method='post' action='modificalinkcartella.php'>
													<input type='hidden' id='Idlinkcartella' name='Idlinkcartella' value='".$rowlink["id"]."'>
													<button type='submit' class='btncartella btn'><i class='fa fa-folder'></i> Modifica</button>
												</form>
												<form method='post' action='cancellalinkcartella.php'>
													<input type='hidden' id='Idlinkcartella' name='Idlinkcartella' value='".$rowlink["id"]."'>
													<button class='btncartella btn' onclick='return checkDelete()'><i class='fa fa-trash-o'></i>Elimina</button>
												</form>";
											}
										echo"
										</div>
										</a>
										<div class='commento '>
											<p>".$rowlink["commento"]."</p>
										</div>
									</div>
									";

								}

								//Visualizzazione sottocartelle
								$sqlsubcart = "SELECT id, nome, commento FROM cartella WHERE padre='".$cartella["id"]."' ORDER BY dataupload ";
								$resultsubcart = $conn->query($sqlsubcart);
								
								while( $rowsubcart = $resultsubcart->fetch_assoc()){
									echo"
									<div class='sottocartella'>	<div class='btnsubcart'>							
										<a class='linkcard' href='#".$rowsubcart["id"]."' >
												<div class='tipoelemento'>
													Sottocartella
												</div>
												<div class='subcartname'>
													<i class='fa fa-folder fa-4x'></i> ".$rowsubcart["nome"]." 
												</div>";
											if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
												echo" 				
												<form method='post' action='modificacartella.php'>
													<input type='hidden' id='Idcartella' name='Idcartella' value='".$rowsubcart["id"]."'>
													<button type='submit' class='btncartella btn'><i class='fa fa-folder'></i> Modifica</button>
												</form>
												<form method='post' action='cancellacartella.php'>
													<input type='hidden' id='Idcartella' name='Idcartella' value='".$rowsubcart["id"]."'>
													<button class='btncartella btn' onclick='return checkDelete()'><i class='fa fa-trash-o'></i>Elimina</button>
												</form>";
											}
												
											echo"	
											</div>
										</a>
										<div class='commento '>
											<p>".$rowsubcart["commento"]."</p>
										</div>
									</div>
									";

								}

								//Visualizzazione elementi
								$sql = "SELECT id, percorso, nome, dataupload, commento FROM programmaallenamento WHERE cartella='".$cartella["id"]."' ORDER BY dataupload ";
								$result = $conn->query($sql);
								while($row = $result->fetch_assoc()) {
									echo "  <h2 class='major'>".$row["nome"]."</h2>
											<div class='bottonischeda'>
												<a download='".$row["percorso"]."' href='".$row["percorso"]."' >
													<button class='btndownload btn'><i class='fa fa-download'></i> Download</button>
												</a>";
												if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
												echo" 
													<form method='post' action='modifica.php'>
														<input type='hidden' id='Idfile' name='Idfile' value='".$row["id"]."'>
														<button type='submit' class='btn'><i class='fa fa-folder'></i> Modifica</button>
													</form>
													<form method='post' action='cancellaelemento.php'>
														<input type='hidden' id='Idfile' name='Idfile' value='".$row["id"]."'>
														<button class='btn' onclick='return checkDelete()'><i class='fa fa-trash-o'></i>Elimina</button>
													</form>";
												}
			
									echo"	</div>
											<div class='containermultimedia'>";
											typeOfFile($row["percorso"]);
									echo"	</span>
											</div>
											<div class='commento '>
												<p>".$row["commento"]."</p>
											</div>";
								 }
								
								//Visualizzazione link elementi
								$sqllink = "SELECT id, elementoriferimento FROM linkprogrammaallenamento WHERE cartellacontenitore='".$cartella["id"]."'";
								$resultlink = $conn->query($sqllink);

								while($rowlink = $resultlink->fetch_assoc()) {
									$sql = "SELECT id, percorso, nome, dataupload, commento FROM programmaallenamento WHERE id='".$rowlink["elementoriferimento"]."'";
									$result = $conn->query($sql);
									while($row = $result->fetch_assoc()) {
										echo "  <h2 class='major'>".$row["nome"]."</h2>
												<div class='bottonischeda'>
													<a download='".$row["percorso"]."' href='".$row["percorso"]."' >
														<button class='btndownload btn'><i class='fa fa-download'></i> Download</button>
													</a>												
													";
													if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
													echo" 
														<form method='post' action='modificalinkelemento.php'>
															<input type='hidden' id='Idlinkfile' name='Idlinkfile' value='".$rowlink["id"]."'>
															<button type='submit' class='btn'><i class='fa fa-folder'></i> Modifica</button>
														</form>
														<form method='post' action='cancellalinkelemento.php'>
															<input type='hidden' id='Idlinkfile' name='Idlinkfile' value='".$rowlink["id"]."'>
															<button class='btn' onclick='return checkDelete()'><i class='fa fa-trash-o'></i>Elimina</button>
														</form>
														<div class='tipoelemento'>
															Collegamento ad un elemento
														</div>";
													}
				
										echo"	</div>											
												<div class='containermultimedia'>";
												typeOfFile($row["percorso"]);
										echo"	</span>
												</div>
												<div class='commento '>
													<p>".$row["commento"]."</p>
												</div>";
									 }
								}






								echo" </article>";
							}
						$conn->close();
						?> 
										

					</div>

					
				<!-- Footer -->
					<footer id="footer">
						<p class="copyright">&copy; Matteo Bordin Corp. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
					</footer>

			</div>


		<!-- BG -->
			<div id="bg"></div>

		 <!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
			<!--<script >
			//  function submitcard(formid) {
			//  	document.getElementById(formid).submit(); 
			//  }
			 </script>
			 <script >
			// var	$body = $('body'),
			// $header = $('#header'),
			// $footer = $('#footer'),
			// $main = $('#main'),
			// $totcards=$('#totcards'),
			// $enterbtncontainer=$('#enterbtncontainer'),
			// $main_articles = $main.children('article'),
			// $article = $main_articles.filter('#' + id);
			
			// alert(cardidjava);
			// if(cardidjava!=null){
			// 	//Hide article, main.
			// 	$article.hide();
			// 	$main.hide();

			// 	// Show footer, header.
			// 	$footer.show();
			// 	$header.show();
			// 	$totcards.show();
			// 	$enterbtncontainer.show();
			// }
			// else{		
			// 	// Hide header, footer.
			// 	$header.hide();
			// 	$footer.hide();
			// 	$totcards.hide();
			// 	$enterbtncontainer.hide();
			// 	// Show main, article.
			// 	$main.show();
			// 	$article.show();		
			// }

			
			</script>-->



			<?php
				function typeOfFile($row) {
					//$type= mime_content_type($row);
					$tmp = explode('.', $row);
					$extension = end($tmp);
					if($extension == "mp4"){
						echo "
						<span class='image main'>
							<video controls='controls' preload='metadata'>
								<source src='".$row."' type='video/mp4'>
							</video>";
					}else if($extension == "png" || $extension == "jpg" || $extension == "jpeg" || $extension == "JPG"){
						echo"
						<span class='image main'>
							<img class='card-img-top' src='".$row."' alt='Image'> ";
					}else if($extension == "pdf"){
						echo"
						<span class='image main'>
						<a download='".$row."' href='".$row."' >
							<img  class='card-img-top ' src='images/pdfIcon.png' alt='Pdf'> 
						</a> ";
					}else if($extension == "xlsx"){
						echo"
						<span class='image main'>
						<a download='".$row."' href='".$row."' >
							<img  class='card-img-top ' src='images/excel.png' alt='excel'> 
						</a> ";
					}

				}


				function percorso($idcart,$mappasito,$conn){//3,tretch//2,fit>stretch//1,padre>fit>stretch
					$sqlmap = "SELECT padre FROM cartella WHERE id=".$idcart." ";
					$resultmap = $conn->query($sqlmap);	
					$rowmap = $resultmap->fetch_assoc();	
					if(is_null($rowmap["padre"])){
						$mappasito="Home>".$mappasito;
						return $mappasito;
					}
					else{						
						  $sqlpadre = "SELECT nome FROM cartella WHERE id=".$rowmap["padre"]." ";
						  $resultpa = $conn->query($sqlpadre);		
						  $rowpadre = $resultpa->fetch_assoc();
						  $mappasito=	$rowpadre["nome"].">".$mappasito;
						  $mappasito=percorso($rowmap["padre"], $mappasito,$conn);		
													
					}
					return $mappasito;
				}

				function getpadre($idcart,$conn){
					$sqlmap = "SELECT padre FROM cartella WHERE id=".$idcart." ";
					$resultmap = $conn->query($sqlmap);	
					$rowmap = $resultmap->fetch_assoc();	
					if(is_null($rowmap["padre"])){
						return "#";
					}
					else{						
						return "#".$rowmap["padre"];														
					}
					
				}

			?> 


	</body>
</html>
