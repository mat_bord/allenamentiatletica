<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Atletica Montebelluna allenamenti</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</head>
	<body >

		<div class="loginwrapper" >
			<div id='enterbtncontainer'>
				<a href='index.php'><button type='button' class=' btn btn-default '>Home</button></a>
			</div>
			<!-- Login -->
			<article class="articolo formcontainer">
				<h2 class="major">Login</h2>
				<form method="post" action="verifica.php">
					<div class="fields">
						<div class="field half">
							<label for="username">Name</label>
							<input type="text" name="username" id="username" />
						</div>
						<div class="field half">
							<label for="password">Password</label>
							<input type="password" name="password" id="password" />
						</div>
					</div>
					<ul class="actions">
						<li><input type="submit" value="Login" class="primary" /></li>
						<li><input type="reset" value="Reset" /></li>
					</ul>
				</form>
			</article>
		
			<footer id="footer">
				<p class="copyright">&copy; Matteo Bordin Corp. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
			</footer>
		</div>

		<!-- BG -->
			<div id="bg"></div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

		

	</body>
</html>
