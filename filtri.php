<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Atletica Montebelluna allenamenti</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="assets/SnackBar/dist/snackbar.css" />
		<script src="assets/SnackBar/dist/snackbar.js"></script>
		<script language="JavaScript" type="text/javascript">
			function checkDelete(){
				return confirm('Sei sicuro di cancellare?');
			}

			function popup(){
				var popupCounts = <?php echo json_encode($_SESSION['Info']); ?>;
				if(popupCounts=='elementocancellato'){
					Snackbar.show({text:'Elemento cancellato', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='elementononcancellato'){
					Snackbar.show({text:'Elemento NON cancellato', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='cartellacreata'){
					Snackbar.show({text:'Cartella creata', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='modificaok'){
					Snackbar.show({text:'Elemento modificato', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='nonmodifica'){
					Snackbar.show({text:'Elemento NON modificato', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='cartellacancellata'){
					Snackbar.show({text:'Cartella cancellata', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='cartellanoncancellata'){
					Snackbar.show({text:'Cartella NON cancellata', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='modificacartellaok'){
					Snackbar.show({text:'Cartella modificata', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='nonmodificacartella'){
					Snackbar.show({text:'Cartella NON modificata', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				<?php $_SESSION['Info']='None'; ?>
			}
				
		</script>


	
	</head>
	<body onload="popup()">
                        

    <div id="wrapper">
        <div id='filtribtncontainer'>
			<a href='index.php'><button type='button' class=' btn btn-default '>Home</button></a>
		</div>
        <div id="filtri">
        <?php
            include 'connectiondb.php';
            echo " <article id='1'>";
            $sql = "SELECT id, percorso, nome, dataupload, commento FROM programmaallenamento WHERE tipo='".$_GET['tipo']."' ORDER BY dataupload ";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) {
                echo "  <h2 class='major'>".$row["nome"]."</h2>
                        <div class='bottonischeda'>
                            <a download='".$row["percorso"]."' href='".$row["percorso"]."' >
                                <button class='btndownload btn'><i class='fa fa-download'></i> Download</button>
                            </a>";
                            if(isset($_SESSION["autorizzato"]) && $_SESSION["autorizzato"] == 1) {
                            echo" 
                                <form method='post' action='modifica.php'>
                                    <input type='hidden' id='Idfile' name='Idfile' value='".$row["id"]."'>
                                    <button type='submit' class='btn'><i class='fa fa-folder'></i> Modifica</button>
                                </form>
                                <form method='post' action='cancellaelemento.php'>
                                    <input type='hidden' id='Idfile' name='Idfile' value='".$row["id"]."'>
                                    <button class='btn' onclick='return checkDelete()'><i class='fa fa-trash-o'></i>Elimina</button>
                                </form>";
                            }

                echo"	</div>
                        <div class='containermultimedia'>";
                        typeOfFile($row["percorso"]);
                echo"	</span>
                        </div>
                        <div class='commento '>
                            <p>".$row["commento"]."</p>
                        </div>";
            }
            echo" </article>";
        $conn->close();
        ?> 
        </div>
                        

    </div>

					
				<!-- Footer -->
					<footer id="footer">
						<p class="copyright">&copy; Matteo Bordin Corp. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
					</footer>

			</div>


		<!-- BG -->
			<div id="bg"></div>

		 <!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>



			<?php
				function typeOfFile($row) {
					$type= mime_content_type($row);

					if(strstr($type, "video/")){
						echo "
						<span class='image main'>
							<video controls='controls' preload='metadata'>
								<source src='".$row."' type='video/mp4'>
							</video>";
					}else if(strstr($type, "image/")){
						echo"
						<span class='image main'>
							<img class='card-img-top' src='".$row."' alt='Image'> ";
					}else if(strstr($type, "application/")){
						echo"
						<span class='image main'>
						<a download='".$row."' href='".$row."' >
							<img  class='card-img-top ' src='images/pdfIcon.png' alt='Pdf'> 
						</a> ";
					}
				}
			?> 


	</body>
</html>
