<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Atletica Montebelluna allenamenti</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="assets/SnackBar/dist/snackbar.css" />
		<script src="assets/SnackBar/dist/snackbar.js"></script>
		<script language="JavaScript" type="text/javascript">

			function popup(){
				var popupCounts = <?php echo json_encode($_SESSION['Info']); ?>;
				if(popupCounts=='errorecreacartella'){
					Snackbar.show({text:'Cartella NON creata', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='errfile'){
					Snackbar.show({text:'File troppo grande', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='elementocaricato'){
					Snackbar.show({text:'Caricamento avvenuto!', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='errelementocaricato'){
					Snackbar.show({text:'Caricamento NON avvenuto', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='linkcaricato'){
					Snackbar.show({text:'Caricamento link avvenuto!', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='linknoncaricato'){
					Snackbar.show({text:'Caricamento link NON avvenuto', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='stessolink'){
					Snackbar.show({text:'Non puoi inserire il collegamento dentro la cartella stessa', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				if(popupCounts=='linkprogrcaricato'){
					Snackbar.show({text:'Caricamento link avvenuto!', showAction: false ,pos: 'top-center',  backgroundColor: '#519657' });
				}
				if(popupCounts=='linkprogrnoncaricato'){
					Snackbar.show({text:'Caricamento link NON avvenuto', showAction: false ,pos: 'top-center',  backgroundColor: '#af4448' });
				}
				<?php $_SESSION['Info']='None'; ?>
			}
				
		</script>
	
	</head>
	<body onload="popup()">

		<div class="loginwrapper" >
			<div id='enterbtncontainer'>
				<a href='index.php'><button type='button' class=' btn btn-default '>Home</button></a>
			</div>
            <!-- Carica -->
            <article class="articolo">
				<h2 class="major">Carica file</h2>
				<form method="post" action="verifyupload.php" enctype="multipart/form-data">
					<div class="fields">
                        <div class="field half">
                            <label for="userfile">Select file to upload:</label>
                            <input type="file" name="userfile" id="userfile" >
                        </div>
						<div class="field half">
							<label for="nome">Nome</label>
							<input type="text" name="nome" id="nome"  maxlength='64' />
                        </div>
                        <div class="field half">
                            <label for="cartella">Cartella</label>
                            <?php 
                                include 'connectiondb.php';
                                
                                $result = $conn->query("SET NAMES 'utf8'");

                                $sql = "SELECT id, nome FROM cartella";
                                $result = $conn->query($sql);
                                echo "<select id='cartella' name='cartella'>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>";                            ?>
                        </div>
                        <div class="field half">
                            <label for="descrizione">Commento</label>
                            <textarea name="commento" id="commento" rows="4"  maxlength='4080'></textarea>
                        </div>
                        <div class="field half">
							<label for="tipo">Tipo</label>
							<select id="tipo" name="tipo">
                                <option value="Programmi">Programmi</option>
                                <option value="Esercizi">Esercizi</option>
                                <option value="Circuiti">Circuiti</option>
                            </select>
                        </div>
					</div>
					<ul class="actions">
						<li><input type="submit" value="Carica" class="primary" /></li>
						<li><input type="reset" value="Reset" /></li>
					</ul>
				</form>


				<h2 class="major">Link file</h2>
				<form method="post" action="verifylinkfileupload.php" enctype="multipart/form-data">
					<div class="fields">
						<div class="field half">
                            <label for="cartella">Elemento da collegare</label>
                            <?php 
                                include 'connectiondb.php';
                                
                                $result = $conn->query("SET NAMES 'utf8'");

                                $sql = "SELECT id, nome FROM programmaallenamento";
                                $result = $conn->query($sql);
                                echo "<select id='programmariferimento' name='programmariferimento'>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>";                            ?>
                        </div>
						<div class="field half">
                            <label for="cartella">Da inserire in </label>
                            <?php 
                                include 'connectiondb.php';
                                
                                $result = $conn->query("SET NAMES 'utf8'");

                                $sql = "SELECT id, nome FROM cartella";
                                $result = $conn->query($sql);
                                echo "<select id='cartellacontenitore' name='cartellacontenitore'>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>";                            ?>
                        </div>
                    </div>
					<ul class="actions">
						<li><input type="submit" value="Carica" class="primary" /></li>
						<li><input type="reset" value="Reset" /></li>
					</ul>
				</form>	

				<h2 class="major">Link cartella</h2>
				<form method="post" action="verifylinkupload.php" enctype="multipart/form-data">
					<div class="fields">
						<div class="field half">
							<label for="nome">Nome link</label>
							<input type="text" name="nome" id="nome"  maxlength='64' />
                        </div>
                        <div class="field half">
                            <label for="cartella">Cartella da collegare</label>
                            <?php 
                                include 'connectiondb.php';
                                
                                $result = $conn->query("SET NAMES 'utf8'");

                                $sql = "SELECT id, nome FROM cartella";
                                $result = $conn->query($sql);
                                echo "<select id='cartellariferimento' name='cartellariferimento'>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>";                            ?>
                        </div>
						<div class="field half">
                            <label for="cartella">Da inserire in </label>
                            <?php 
                                include 'connectiondb.php';
                                
                                $result = $conn->query("SET NAMES 'utf8'");

                                $sql = "SELECT id, nome FROM cartella";
                                $result = $conn->query($sql);
                                echo "<select id='cartellacontenitore' name='cartellacontenitore'>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>";                            ?>
                        </div>
                        <div class="field half">
                            <label for="descrizione">Commento</label>
                            <textarea name="commento" id="commento" rows="4"  maxlength='4080'></textarea>
                        </div>
                    </div>
					<ul class="actions">
						<li><input type="submit" value="Carica" class="primary" /></li>
						<li><input type="reset" value="Reset" /></li>
					</ul>
				</form>				






            </article>


            <h2 class="major" >Oppure</h2>

			<!-- Crea cartella -->
			<article class="articolo">
				<h2 class="major">Crea cartella</h2>
				<form method="post" action="creacartella.php">
					<div class="fields">
						<div class="field half">
							<label for="nome">Nome</label>
							<input type="text" name="nome" id="nome"  maxlength='64' />
						</div>
						<div class="field">
                            <label for="commento">Descrizione</label>
                            <textarea name="commento" id="commento" rows="4"  maxlength='2040'></textarea>
                        </div>
						<div class="field half">
                            <label for="sottocartella">Sottocartella in:</label>
							                            <?php 
                                include 'connectiondb.php';
                                
                                $result = $conn->query("SET NAMES 'utf8'");

                                $sql = "SELECT id, nome FROM cartella";
                                $result = $conn->query($sql);
								echo "<select id='sottocartella' name='sottocartella'>
									  <option value='NULL'>Nulla</option>";
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>";                            ?>
                        </div>	
					</div>
					<ul class="actions">
						<li><input type="submit" value="Crea" class="primary" /></li>
						<li><input type="reset" value="Reset" /></li>
					</ul>
				</form>
            </article>


		
			<footer id="footer">
				<p class="copyright">&copy; Matteo Bordin Corp. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
			</footer>
		</div>

		<!-- BG -->
			<div id="bg"></div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
            

		

	</body>
</html>
