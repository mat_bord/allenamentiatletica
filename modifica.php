<?php
	session_start();
?>
<!DOCTYPE HTML>
<!--
	Dimension by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Atletica Montebelluna allenamenti</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	</head>
	<body >

		<div class="loginwrapper" >
			<div id='enterbtncontainer'>
				<a href='index.php'><button type='button' class=' btn btn-default '>Home</button></a>
			</div>
            <!-- Carica -->
            <article class="articolo">
				<h2 class="major">Modifica</h2>
				<form method="post" action="verificamodifiche.php" enctype="multipart/form-data">
					<div class="fields">
                    <?php 
                        include 'connectiondb.php';
                        $resultm = $conn->query("SET NAMES 'utf8'");
                        $sqlm = "SELECT id, nome, tipo, commento, cartella FROM programmaallenamento WHERE id=".$_POST["Idfile"]."";
                        $resultm = $conn->query($sqlm);
                        $rowm = $resultm->fetch_assoc();

                        echo "
                        <input type='hidden' id='idfile' name='idfile' value='".$_POST["Idfile"]."'>
                        <div class='field half'>
							<label for='nome'>Nome</label>
							<input type='text' name='nome' id='nome' value='".$rowm["nome"]."'  maxlength='64'/>
                        </div>
                        <div class='field half'>
                            <label for='cartella'>Cartella</label>";
                            
                                $sqloriginale = "SELECT nome FROM cartella WHERE id=".$rowm["cartella"]."";
                                $resultoriginale = $conn->query($sqloriginale);
                                $roworiginale = $resultoriginale->fetch_assoc();

                                $sql = "SELECT id, nome FROM cartella";
                                $result = $conn->query($sql);

                                echo "<select id='cartella' name='cartella'>
                                <option value='".$rowm["cartella"]."'>".$roworiginale["nome"]."</option>";

                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    echo"
                                        <option value='".$row["id"]."'>".$row["nome"]."</option>
                                    ";
                                }
                                echo"</select>
                        </div>
                        <div class='field half'>
                            <label for='descrizione'>Commento</label>

                            <textarea name='commento' id='commento' rows='4'  maxlength='4080'> ".$rowm["commento"]."</textarea>
                        </div>
                        <div class='field half'>
							<label for='tipo'>Tipo</label>
                            <select id='tipo' name='tipo'>
                            <option value='".$rowm["tipo"]."'>".$rowm["tipo"]."</option>";                            
                            ?>
                                <option value="Programmi">Programmi</option>
                                <option value="Esercizi">Esercizi</option>
                                <option value="Circuiti">Circuiti</option>
                            </select>
                        </div>	
					</div>
					<ul class="actions">
						<li><input type="submit" value="Salva modifiche" class="primary" /></li>
					</ul>
				</form>
            </article>



            <footer id="footer">
				<p class="copyright">&copy; Matteo Bordin Corp. Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
			</footer>
		</div>

		<!-- BG -->
			<div id="bg"></div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
            

		

	</body>
</html>
